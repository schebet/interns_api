const Intern = use('App/Models/Intern')
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class FindIntern {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, response, params: { id } }, next) {
    const intern = await Intern.find(id)

    if (!intern) {
      return response.status(404).json({
        message: "intern not found"
      })
    }

    request.body.intern = intern
    await next()
  }
}

module.exports = FindIntern
