'use strict'
const Intern = use('App/Models/Intern')
const Database = use('Database')


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with interns
 */
class InternController {
  /**
   * Show a list of all interns.
   * GET interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const all = await Intern.all()


    response.json({
      message: "Here's your peeps",
      data: all
    })

  }

  /**
   * Render a form to be used for creating a new intern.
   * GET interns/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new intern.
   * POST interns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

    const { name, stack, email } = request.only(['name', 'stack', 'email'])
    const stackjson = JSON.stringify(stack)
    let intern = new Intern()
    intern.name = name
    intern.stack = stackjson
    intern.email = email
    await intern.save()
    response.ok.json({
      message: "successfully created intern",
      data: intern
    })

  

  }

  /**
   * Display a single intern.
   * GET interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, response, view }) {

    const intern = request.post().intern

    response.status(200).json({
      message: "Intern",
      data: intern
    })
  }

  /**
   * Render a form to update an existing intern.
   * GET interns/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update intern details.
   * PUT or PATCH interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const intern = request.post().intern
   
    if (intern) {
      const { name, email, stack } = request.post();
      const stackjson = JSON.stringify(stack)
      intern.name = name
      intern.email = email
      intern.stack = stackjson

      await intern.save()

      response.status(200).json({
        message: "successfully updated intern's details",
        data: intern
      })
    }

  }

  /**
   * Delete a intern with id.
   * DELETE interns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const intern = request.post().intern
   
    if (intern) {
      await intern.delete()

      response.status(200).json({
        message: "successfully deleted intern",
        
      })
    }
  }
}

module.exports = InternController
