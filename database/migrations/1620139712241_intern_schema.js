'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InternSchema extends Schema {
  up () {
    this.create('interns', (table) => {
      table.increments()
      table.string('name', 80).notNullable()
      table.string('email', 254).notNullable()
      table.string('stacks').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('interns')
  }
}

module.exports = InternSchema
